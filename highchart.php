<!DOCTYPE HTML>


<html>
	

	<head>
		


		
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		
		
	<script type="text/javascript">
$(function () {

	var xAxisString = "<?php echo $_POST["xValues"]; ?>";
	var xAxisValues = xAxisString.split('|');
	var yAxisValue1String = "<?php echo $_POST["yValues1"]; ?>";
	var yAxisValue1 = yAxisValue1String.split('|');
	for(var i=0; i<yAxisValue1.length; i++) { yAxisValue1[i] = +yAxisValue1[i]; } 
	var yAxisValue2String = "<?php echo $_POST["yValues2"]; ?>";
	var yAxisValue2 = yAxisValue2String.split('|');
	for(var i=0; i<yAxisValue2.length; i++) { yAxisValue2[i] = +yAxisValue2[i]; } 
	
	
    $('#container').highcharts({
        
		chart: {
type: 'areaspline'
        },
        
        title: {text: '<?php echo $_POST["title"]; ?>'},
		legend: {
layout: 'vertical',
            
		align: 'left',
 verticalAlign: 'top',
 x: 150,
 y: 100,
 floating: true,
 borderWidth: 1,
          
        },
        
		xAxis: {
title: {text: '<?php echo $_POST["xAxis"]; ?>'}, categories: xAxisValues,
            
		plotBands: [{
 from: 13,
 to: 24,
 color: 'rgba(229, 232, 232, .2)'
}]
},
        
		yAxis: {
title: {text: '<?php echo $_POST["yAxis"]; ?>'}},
        
		tooltip: {
shared: true,
 valueSuffix: ' units'
},
        
		credits: {
enabled: false
},
        
		plotOptions: {
areaspline: {
fillOpacity: 0.3
}
},
        
		series: [{
name: '<?php echo $_POST["yAxisValue1Name"]; ?>',
 color: 'rgb(0, 150, 214)', marker: {enabled: false}, data: yAxisValue1
}, 
			 {
name: '<?php echo $_POST["yAxisValue2Name"]; ?>',
 color: 'rgb(107, 58, 151)', marker: {enabled: false}, data: yAxisValue2
}]
    });
});
		
	</script>
	
	</head>
	
	<body>
	
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js">
	</script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

	
	</body>

</html>
