<html>
	<head>
		<style type="text/css">
			textarea{
			 vertical-align: middle;
			}
			
			label {
			     display: inline-block;
			    width: 240px;
			    text-align: center;
			    background-color: lightgreen;
			    -webkit-border-radius: 5px;
			    border-radius: 5px;
			    padding: 5px;
			    margin: 10px;
			    font-size: larger;
			}
			
			input[type=submit] {
			    padding:5px 15px; 
			    background:lightblue; 
			    border:0 none;
			    cursor:pointer;
			    -webkit-border-radius: 5px;
			    border-radius: 5px; 
			    margin-left: 480px;
			    font-size: larger;
			}​
		</style>
	</head>
	<body>
		<form action="highchart.php" method="post">
		   <label for="title">Title: </label><input type="text" style="width:315px;" name="title" value="Value Management Office - Breakeven Chart"/>
		   <br>
		   <label for="xAxis">xAxis: </label><input type="text" name="xAxis" value="Month" />
		   <br>
		   <div><label for="xValues">xAxis Values: </label><textarea rows="2" cols="50" name="xValues">0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36</textarea> </div>
		   <label for="yAxis">yAxis: </label><input type="text" name="yAxis" value="Amount" />
		   <br>
		   <label for="yAxisValue1Name">yAxis Value Set 1 Name: </label><input type="text" name="yAxisValue1Name" value="Return" />
		   <br>
		   <div><label for="yValues1">yAxis Value Set 1: </label><textarea rows="6" cols="50" name="yValues1">0|219238|438477|657715|876954|1096192|1315430|1534669|1753907|1973146|2192384|2411622|2630861|2860799|3090737|3320675|3550614|3780552|4010490|4240428|4470366|4700305|4930243|5160181|5390119|5631082|5872045|6113007|6353970|6594933|6835895|7076858|7317820|7558783|7799746|8040708|8281671</textarea></div>
		   <label for="yAxisValue2Name">yAxis Value Set 2 Name: </label><input type="text" name="yAxisValue2Name" value="Investment" />
		   <br>
		   <div><label for="yValues2">yAxis Value Set 2: </label><textarea rows="6" cols="50" name="yValues2">1780000|1780000|1780000|1780000|1780000|1780000|1780000|1780000|1780000|1780000|1780000|1780000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|1990000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000|2200000</textarea></div>
		   <br>
		   <input type="submit" name="submit" value="Generate Chart" />
		</form>
	</body>
</html>